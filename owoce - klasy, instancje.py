class Owoce:
    def __init__(self, rodzaj, kolor):
        self.rodzaj = rodzaj
        self.kolor = kolor

    def opis(self):
        return(f"To {self.rodzaj}, ma kolor {self.kolor}.")
    
owoc1 = Owoce("owoc","czerwony")
print(owoc1.rodzaj)
print(owoc1.kolor)
print(owoc1.opis())
print('\n ')

class Jabłko(Owoce):
    def __init__(self, smak):
        self.smak = smak
        super().__init__('jabłko', 'biały')
       
jabłko1 = Jabłko('kwaśny')
print(jabłko1.rodzaj)
print(jabłko1.kolor)
print(f"{jabłko1.opis()} Ma {jabłko1.smak} smak.")
print('\n ')

class Gruszka(Owoce):
        def __init__(self, wielkość):
            self.wielkość = wielkość
            super().__init__('gruszka', 'zielony') 

gruszka1 = Gruszka('duża')
print(gruszka1.rodzaj)
print(gruszka1.kolor)
print(f"{gruszka1.opis()} Jest {gruszka1.wielkość}.")
