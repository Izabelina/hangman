# sposoby importowania modulu:

'''
import <module>
Import all of <module>’s namespace into the name <module>.
Import module names can be accessed from the calling module
with <module>.<name>.

import <module> as <other_name>
Import all of <modules>’s namespace into the name <other_name>. Import module
names can be accessed from the calling module with <other_name>.<name>.

from <module> import <name1>, <name2>, ...
Import only the names <name1>, <name2>, etc, from <module>. The names are added
to the calling modules’s local namespace and can be accessed directly.
'''

# 1.

#import <module>
##import adder
##
##value = adder.add(2,2)
##print(value)

# 2.

#import <module> as <other_name>
#dostęp do przestrzeni nazw modułu
#uzyskuje się poprzez <other_name> zamiast <module>.

##import adder as a
##
##value = a.add(2,2)
##double_value = a.double(value)
##print(double_value)

# 3.

# from <module> import <name>

##from adder import add, double
##
##value = add(2, 2)
##double_value = double(value)
##print(double_value)

import greeter




