# main.py

# import mypackages.module1
# import mypackages.module2

# mypackages.module1.greet("Iza")
# mypackages.module2.depart("Iza")

'''
Istnieją trzy odmiany instrukcji importu, których się nauczyłeś
importowanie nazw z modułów. Te trzy odmiany przekładają się na
następujące cztery warianty importowania modułów z pakietów:

1. import <package>
2. import <package> as <other_name>
3. from <package> import <module>
4. from <package> import <module> as <other_name>
5. from <package>.<module> import <name>
'''
# from mypackages import module1, module2
# module1.greet("Iza")
# module2.depart("Pythonista")

# from mypackages import module1 as m1, module2 as m2
# m1.greet("Iza")
# m2.depart("Pythonista")

# from mypackages.module1 import greet
# from mypackages.module2 import depart
# greet("Iza")
# depart("Pythonista")
'''
# General, the following format is the most explicit:
    #import <package>.<module>
#Then, to access names from module, you need to type something like the following
    #<package>.<module>.<name>
'''
from mypackages.module1 import greet
from mypackages.mysubpackage.module3 import people


for person in people:
    greet(person)
